# encoding: utf-8

class SessionsController < ApplicationController

    def callback
        reset_session
        auth = request.env["omniauth.auth"]
        #user = User.find_by_provider_and_uid(auth["provider"],auth["uid"]) || User.create_with_omniauth(auth)
        #session[:user_id] = user.id
        session[:auth] = auth
        session[:profile] = auth.info
        session[:credentials] = auth.credentials
        #session[:link] = auth.

        #binding.pry
        redirect_to root_url, :notice => "ログインしました"
    end
    
    def destroy
        session[:auth] = nil
        session[:profile] = nil
        session[:credentials] = nil
        redirect_to root_url, :notice => "ログアウトしました"
    end
end
